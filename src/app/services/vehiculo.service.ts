import { Injectable } from '@angular/core';
import { Vehiculo } from '../model/vehiculo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  private urlEndPoint: String = 'http://localhost:3001/api/';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getVehiculos(): Observable<Vehiculo[]> {
    return this.http.get<Vehiculo[]>(this.urlEndPoint + 'vehiculos');
  }

  create(vehiculo: Vehiculo): Observable<Vehiculo> {
    return this.http.post<Vehiculo>(this.urlEndPoint + 'crearVehiculo', vehiculo, {headers: this.httpHeaders});
  }

  getVehiculo(id): Observable<Vehiculo> {
    return this.http.get<Vehiculo>(`${this.urlEndPoint}` + 'vehiculos' + `/${id}`);
  }

  update(vehiculo: Vehiculo): Observable<Vehiculo> {
    return this.http.put<Vehiculo>(`${this.urlEndPoint}` + 'editarVehiculo' + `/${vehiculo.id}`, vehiculo, {headers: this.httpHeaders});
  }

  getVehiculosSearch(busqueda: string): Observable<Vehiculo[]> {
      return this.http.get<Vehiculo[]>(this.urlEndPoint + 'vehiculosSearch' + `/${busqueda}`);
  }


}
