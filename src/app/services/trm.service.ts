import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tasa } from '../model/tasa';

@Injectable({
  providedIn: 'root'
})
export class TrmService {

  private urlEndPoint: String = 'http://localhost:9090/api/';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getTasa() {
    return this.http.get<Tasa>(this.urlEndPoint + 'tasa');
  }

}
