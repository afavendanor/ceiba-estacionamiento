import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Factura } from '../model/factura';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private urlEndPoint: String = 'http://localhost:3001/api/';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getFacturas(): Observable<Factura[]> {
    return this.http.get<Factura[]>(this.urlEndPoint + 'facturas');
  }

  create(id): Observable<Factura> {
    return this.http.post<Factura>(`${this.urlEndPoint}` + 'facturar' + `/${id}`, {headers: this.httpHeaders});
  }

  getFactura(id): Observable<Factura> {
    return this.http.get<Factura>(`${this.urlEndPoint}` + 'facturas' + `/${id}`);
  }

}
