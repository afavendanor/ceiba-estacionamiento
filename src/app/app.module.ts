import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from '../app/views/header/header.component';
import { FooterComponent } from '../app/views/footer/footer.component';
import { VehiculosComponent } from '../app/views/vehiculos/vehiculos.component';

import { VehiculoService } from '../app/services/vehiculo.service';
import { FacturaService } from '../app/services/factura.service';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { VehiculoFormComponent } from '../app/views/forms/vehiculo-form/vehiculo-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FacturasComponent } from '../app/views/facturas/facturas.component';
import { FacturacionComponent } from './views/facturacion/facturacion.component';
import { TrmComponent } from './views/trm/trm.component';
import { TrmService } from './services/trm.service';

const routes = [
  {path: '', redirectTo: '/vehiculos', pathMatch: 'full'},
  {path: 'vehiculos', component: VehiculosComponent},
  {path: 'vehiculos/form', component: VehiculoFormComponent},
  {path: 'vehiculos/form/:id', component: VehiculoFormComponent},
  {path: 'facturas', component: FacturasComponent},
  {path: 'facturas/:id', component: FacturacionComponent},
  {path: 'tasa', component: TrmComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    VehiculosComponent,
    VehiculoFormComponent,
    FacturasComponent,
    FacturacionComponent,
    TrmComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [VehiculoService, FacturaService, TrmService],
  bootstrap: [AppComponent]
})
export class AppModule { }
