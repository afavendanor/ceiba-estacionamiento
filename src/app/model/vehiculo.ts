export class Vehiculo {
    id: number;
    placa: String;
    tipo: String;
    cilindraje: number;
    fechaIngreso: Date;
}
