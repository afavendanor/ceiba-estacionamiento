export class Factura {
    id: number;
    placa: string;
    totalApagar: number;
    fechaIngreso: Date;
    fechaSalida: Date;
}
