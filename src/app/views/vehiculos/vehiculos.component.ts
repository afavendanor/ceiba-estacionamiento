import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../../model/vehiculo';
import { VehiculoService } from '../../services/vehiculo.service';
import {Router, ActivatedRoute} from '@angular/router';
import { FacturaService } from '../../services/factura.service';
import swal from 'sweetalert2';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {

  vehiculos: Vehiculo[] = [];
  campoBusqueda: FormControl;
  busqueda: string;

  cargando = false;
  resultados = false;
  noresultados = false;

  constructor(private vehiculoService: VehiculoService,
    private facturaService: FacturaService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.campoBusqueda = new FormControl();
    this.campoBusqueda.valueChanges.subscribe(
      term => {
        this.busqueda = term;
        this.cargando = true;
        if (this.busqueda.length !== 0) {
          this.vehiculoService.getVehiculosSearch(this.busqueda).subscribe(
            vehiculos => {
              this.vehiculos = [];
              for (const id$ in vehiculos) {
                if (vehiculos.hasOwnProperty(id$)) {
                  const v = vehiculos[id$];
                  this.vehiculos.push(vehiculos[id$]);
                }
              }
              if (this.vehiculos.length < 1 && this.busqueda.length >= 1) {
                this.noresultados = true;
              } else {
                this.noresultados = false;
              }
            });
          this.cargando = false;
          this.resultados = true;
        } else {
          this.vehiculoService.getVehiculos().subscribe(
            vehiculos => this.vehiculos = vehiculos
          );
        }
      }
    );
    this.vehiculoService.getVehiculos().subscribe(
      vehiculos => this.vehiculos = vehiculos
    );
  }

  createFactura(id): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea retirar el vehiculo del parqueadero?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, retirar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.facturaService.create(id).subscribe( factura => {
          this.router.navigate(['/facturas/' + factura.id]);
        });
      }
    });
  }
}
