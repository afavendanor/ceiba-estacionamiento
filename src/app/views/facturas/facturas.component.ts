import { Component, OnInit } from '@angular/core';
import { Factura } from '../../model/factura';
import { FacturaService } from '../../services/factura.service';

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.css']
})
export class FacturasComponent implements OnInit {

  facturas: Factura[] = [];

  constructor(private facturaService: FacturaService) { }

  ngOnInit() {
    this.facturaService.getFacturas().subscribe(
      facturas => this.facturas = facturas
    );
  }

}
