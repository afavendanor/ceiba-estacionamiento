import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { FacturaService } from '../../services/factura.service';
import { Factura } from '../../model/factura';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.css']
})
export class FacturacionComponent implements OnInit {

    private factura: Factura = new Factura();

  constructor(private facturaService: FacturaService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarFactura();
  }

  cargarFactura(): void {
      this.activatedRoute.params.subscribe(params => {
        const id = params['id'];
        if (id) {
          this.facturaService.getFactura(id).subscribe( (factura) => this.factura = factura);
        }
      });
  }

  printToCart(printSectionId) {
    const innerContents = document.getElementById(printSectionId).innerHTML;
    // tslint:disable-next-line:max-line-length
    const popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    // tslint:disable-next-line:max-line-length
    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
    popupWinindow.document.close();
  }
}
