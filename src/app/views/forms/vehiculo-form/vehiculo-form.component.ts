import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../../../model/vehiculo';
import { VehiculoService } from '../../../services/vehiculo.service';
import {Router, ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-vehiculo-form',
  templateUrl: './vehiculo-form.component.html',
  styleUrls: ['./vehiculo-form.component.css']
})
export class VehiculoFormComponent implements OnInit {

  private vehiculo: Vehiculo = new Vehiculo();
  private titulo: String = 'Ingresar Vehiculo';

  constructor(private vehiculoService: VehiculoService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {

    }

  ngOnInit() {
    this.cargarVehiculo();
  }

  cargarVehiculo(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.vehiculoService.getVehiculo(id).subscribe( (vehiculo) => this.vehiculo = vehiculo);
      }
    });
  }

  create(): void {
    this.vehiculoService.create(this.vehiculo)
    .subscribe( vehiculo => {
      this.router.navigate(['/vehiculos']);
      swal('Vehiculo Ingresado', `Vehiculo ingresado con éxito!`, 'success');
      },
      error => {
        console.log(error);
        swal('Error', error.error, 'error');
    });
  }

  update(): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea modificar los datos del vehiculo?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, editar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.vehiculoService.update(this.vehiculo)
        .subscribe( vehiculo => {
          this.router.navigate(['/vehiculos']);
          swal('Vehiculo Actualizado', `Vehiculo actualizado con éxito!`, 'success');
        },
        error => {
          console.log(error);
          swal('Error', error.error, 'error');
        });
      }
    });
  }
}
