import { Component, OnInit } from '@angular/core';
import { TrmService } from '../../services/trm.service';
import { Tasa } from '../../model/tasa';

@Component({
  selector: 'app-trm',
  templateUrl: './trm.component.html',
  styleUrls: ['./trm.component.css']
})
export class TrmComponent implements OnInit {

  tasa: String;

  constructor(private trmService: TrmService) { }

  ngOnInit() {
    this.trmService.getTasa().subscribe(
       (data: Tasa) => this.tasa = data['valor']
    );
  }

}
